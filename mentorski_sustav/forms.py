from django import forms
from django.forms import ModelForm
from .models import Upisi, User, Predmeti
from django.db.models import Sum, Count

	

class UpisiForm(ModelForm):
	class Meta:
		model = Upisi
		fields = ['student', 'predmet', 'status' ]
		labels = { 'status': 'Status', }
		widgets = {
			'status': forms.Select(attrs={'style': 'width: 200px'}),
			'status': forms.HiddenInput(),
		}

	def queryset(self, request):
		if request.user.is_superuser:
			return User.objects.filter(is_superuser=False)
		return User.objects.all() 

	def __init__(self, student=None,*args, **kwargs):  
		super(UpisiForm, self).__init__(*args, **kwargs)
		if student:
			self.fields['student'].queryset = User.objects.exclude(pk=14)   #da mi ne vraca ime administatora u upisnoj formi, bitno je na view stavit trenutno usera u formu
			#self.fields['predmet'].queryset = Predmeti.objects.exclude(upisi__student__in=[9,10])  ##hardcode radi
			self.fields['predmet'].queryset = Predmeti.objects.exclude(upisi__status="polozen") #izbacivanje svih polozenih predmeta s liste


class UpisPredmetaForm(ModelForm):
	class Meta:
		model = Upisi
		fields = ['student', 'predmet', 'status' ]

		labels = { 'status': 'Status', }
		widgets = {
			'student': forms.HiddenInput(),
			'status': forms.HiddenInput(),
		}

	def __init__(self, student=None, *args, **kwargs):   #, student=None
	    super(UpisPredmetaForm, self).__init__(*args, **kwargs)
	    if student:
	    	#self.fields['predmet'].queryset = Predmeti.objects.exclude(upisi__student=student) 
	    	#napkon radi, filtrira mi sve predmete ovisno o tome koji su upisani na određenog(prijavljenog) studenta
	    	qs = Upisi.objects.filter(predmet_id=7, student_id=student, status="nepolozen")    #ako predmet Analiza 1 nije polozen onda ne moze upisat Analizu 2
	    	qs1 = Predmeti.objects.filter(upisi__status="polozen", upisi__student_id=student, sem_redovni=1).aggregate(sum_all=Sum('bodovi')).get('sum_all') 
	    	#if qs.exists():
	    	if qs1 < 20: 
        		self.fields['predmet'].queryset = Predmeti.objects.exclude(pk=28) #izbacivanje analize 2
