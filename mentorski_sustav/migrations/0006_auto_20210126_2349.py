# Generated by Django 3.1.4 on 2021-01-26 22:49

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mentorski_sustav', '0005_auto_20210125_2359'),
    ]

    operations = [
        migrations.AlterField(
            model_name='upisi',
            name='predmet',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='mentorski_sustav.predmeti'),
        ),
        migrations.AlterField(
            model_name='upisi',
            name='student',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
    ]
