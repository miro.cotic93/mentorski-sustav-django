from django.urls import path
from . import views
from . views import (
	PredmetiListView,
	PredmetiDetailView,
    PredmetiCreateView,
    PredmetiUpdateView,
    UpisiUpdateView,
    PredmetiDeleteView,
    StudentiListView,
    StudentiDetailView,
    SemestriDetailView,
    UserUpdateView,
    UserDeleteView,
    ISPIT,
    ISPITbroj,
    ISPITpolozeni,
    ISPITimena,
    SemestriPKListView,
    SemestriRedovniListView,
    SemestriIzvanredniListView,
    SemestriPK2ListView,
    PopisPolozenihView,
    PopisSvihView,
    PopisNepolozenihView,
    PopisIzbornihView,
    UserCreateView,
    IspisStudentaView,
    IspisDeleteView,
    UpisPredmetaView,
    UpisStudentaView,
    UkupnaSumaECTSListView,
    StudentKriterijListView
)


urlpatterns = [
     #DETAIL
     path('mentorski_sustav/<int:pk>/predmeti/', PredmetiDetailView.as_view(), name='mentorski-predmeti_detail'),
     path('mentorski_sustav/<int:pk>/semestri/', SemestriDetailView.as_view(), name='mentorski-semestri_detail'),
     path('mentorski_sustav/<int:pk>/user/', StudentiDetailView.as_view(), name='mentorski-user_detail'),  
     path('mentorski_sustav/<int:pk>/ISPITbroj/', ISPITbroj.as_view(), name='mentorski-ISPIT_broj'),
     path('mentorski_sustav/<int:pk>/ISPIT/', ISPIT.as_view(), name='mentorski-ISPIT'),

     #CREATE
     path('mentorski_sustav/new_user/', UserCreateView.as_view(), name='mentorski-user_create'),
     path('mentorski_sustav/new/', PredmetiCreateView.as_view(), name='mentorski-predmti_create'),
     path('mentorski_sustav/upis_studenta/', UpisStudentaView.as_view(), name='mentorski-student_upis'),  #kad ide u detalje <int:pk>/
     path('mentorski_sustav/upis_predmeta/', UpisPredmetaView.as_view(), name='mentorski-predmet_upis'),
     
     #UPDATE
     path('mentorski_sustav/<int:pk>/update/', PredmetiUpdateView.as_view(), name='mentorski-predmeti_update'),
     path('mentorski_sustav/<int:predmet_id>/', UpisiUpdateView.as_view(), name='mentorski-predmeti_status'),  #promjena statusa RADI
     path('mentorski_sustav/<int:pk>/update/user/', UserUpdateView.as_view(), name='mentorski-update'),

     #DELETE
     path('mentorski_sustav/ispis_studenta/<int:predmet_id>/', IspisStudentaView.as_view(), name='mentorski-ispis_studenta'),  #ispis studenta od strane MENTORA
     path('mentorski_sustav/<int:predmet_id>/ispis/', IspisDeleteView.as_view(), name='mentorski-predmeti_ispis'),  #ispis predmeta RADI
     path('mentorski_sustav/<int:pk>/delete/user/', UserDeleteView.as_view(), name='mentorski-delete'),
     path('mentorski_sustav/<int:pk>/delete/', PredmetiDeleteView.as_view(), name='mentorski-predmeti_delete'),
     
     #LISTVIEW
     path('mentorski_sustav/imena/', ISPITimena.as_view(), name='mentorski-imena'),
     path('mentorski_sustav/linearna/', ISPITpolozeni.as_view(), name='mentorski-linearna'),
     path('predmeti/', PredmetiListView.as_view(), name='mentorski-predmeti'),
     path('izborni/', PopisIzbornihView.as_view(), name='mentorski-predmeti_izborni'),
     path('mentorski_sustav/ECTS_ukupno/', UkupnaSumaECTSListView.as_view(), name='mentorski-suma_ECTS'),
     path('mentorski_sustav/student_kriterij/', StudentKriterijListView.as_view(), name='mentorski-student_ECTS'),
     path('mentorski_sustav/polozeni/', PopisPolozenihView.as_view(), name='mentorski-predmeti_polozeni'),
     path('mentorski_sustav/nepolozeni/', PopisNepolozenihView.as_view(), name='mentorski-predmeti_nepolozeni'),
     path('mentorski_sustav/svi/', PopisSvihView.as_view(), name='mentorski-predmeti_svi'),
     path('semestri_izvanredni/', SemestriIzvanredniListView.as_view(), name='mentorski-izvanredni'),
     path('semestri_redovni/', SemestriRedovniListView.as_view(), name='mentorski-redovni'),
     path('semestri_izvanredni/<int:pk>', SemestriPKListView.as_view(), name='mentorski-semestriPK'),
     path('semestri_redovni/<int:pk>', SemestriPK2ListView.as_view(), name='mentorski-semestriPK2'),
     path('user/', StudentiListView.as_view(), name='mentorski-user'),
     path('', views.home, name='mentorski-home'),
]



