from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from django.urls import reverse
from django.db.models import Sum
from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.forms import ValidationError
from django.db import migrations
from django.urls import reverse_lazy
from django import forms
from django.core.exceptions import ValidationError

class User(AbstractUser):
    MENTOR = 'Mentor'
    STUDENT = 'Student'
    ROLE=(
        (MENTOR, ('Mentor')),
        (STUDENT, ('Student')),
    )
    role = models.CharField(('role'), default='Student', max_length=25, choices=ROLE) 

    NON = 'None'
    REDOVNI = 'Redovni'   
    IZVANDREDNI = 'Izvanredni'
    STATUS=(
        (NON, ('None')),
        (REDOVNI, ('Redovni')),
        (IZVANDREDNI, ('Izvanredni')),
    )
    status = models.CharField(('status'), default='None', max_length=25, choices=STATUS) 

    def queryset(self, request):
        qs = super(User, self).get_queryset(request)
        if request.user.is_superuser:
            return User.objects.exclude(pk=14)
        else:
            return qs

    def get_absolute_url(self):  #da mogu vratit nazad nakon update-a
        return reverse('mentorski-user_detail', kwargs={'pk': self.pk})



class Predmeti(models.Model):
    ime = models.CharField(max_length=255) #, Unique=True
    kod = models.CharField(unique=True, max_length=16)
    program = models.CharField(max_length=36)
    bodovi = models.IntegerField(null=True, blank=True)
    sem_redovni = models.IntegerField(null=True, blank=True)
    sem_izvanredni = models.IntegerField(null=True, blank=True)
    DA = 'Da'
    NE = 'Ne'
    IZBORNI=(
        (DA, ('Da')),
        (NE, ('Ne')),
    )
    izborni = models.CharField(('izborni'), default='Ne', max_length=5, choices=IZBORNI)   #enum   enum('da','ne')

    def get_absolute_url(self):  #da mogu vratit nazad nakon update-a
        return reverse('mentorski-predmeti_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.ime #da mi vrati točan naziv predmeta



class Upisi(models.Model):
    student = models.ForeignKey(User, on_delete=models.SET_NULL, null=True) 
    predmet = models.ForeignKey(Predmeti, on_delete=models.SET_NULL, null=True)
    STATUS_CHOICES = (
        ('polozen', 'polozen'),
        ('nepolozen', 'nepolozen'),
    )
    status = models.CharField(max_length=10, default="nepolozen", choices = STATUS_CHOICES)  

    def validate_unique(self, exclude=None):   
        qs1 = Upisi.objects.filter(predmet_id=self.predmet_id, student_id=self.student_id, status=self.status)
        #qs1 = Upisi.objects.filter(predmet_id=self.predmet_id, student_id=self.student_id)
        #qs2 = Upisi.objects.filter(predmet_id=self.predmet_id, student_id=self.student_id, status="polozen")
        #qs = Upisi.objects.filter(predmet_id=5, student_id=self.student_id, status="nepolozen")
        if qs1.exists() or self.student_id == 14: 
            raise ValidationError('Odabrani status već postoji u sustavu!')

    def save(self, *args, **kwargs):
        #self.validate_unique()
        super(Upisi, self).save(*args, **kwargs)

    def get_absolute_url(self):  #da mogu vratit nazad nakon update-a
        return reverse('mentorski-predmeti_detail', kwargs={'pk': self.predmet.id})

    def __str__(self):
        return str(self.predmet.ime)   #mora sam stavit str kad sam pristupa id iz druge ili čak i iste tablice da mi konverta
        #return self.predmet_id


    #def queryset(self, request):
     #   if request.user.is_superuser:
            #return User.objects.all()
            #return User.objects.filter(is_superuser=False)
      #      return User.objects.exclude(pk=14) 
     #   return User.objects.all() 