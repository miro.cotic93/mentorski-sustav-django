from django.contrib import admin
from .models import Predmeti
from .models import Upisi
from .models import User

admin.site.register(Predmeti)
admin.site.register(Upisi)
admin.site.register(User)