from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from .models import Predmeti, User, Upisi     
from django.db.models import F
from django.db.models import Sum, Count
from django.db import models
from django.template.defaulttags import register
from django.views.decorators.csrf import csrf_protect
from django.views.generic.edit import FormMixin
from .forms import UpisiForm, UpisPredmetaForm
from django.urls import reverse
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.messages import constants as messages
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.forms import ValidationError
from django.db.models import Count
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)



def home(request):
	return render(request, 'mentorski_sustav/home.html')

	
class PopisPolozenihView(ListView):   
	model = Upisi
	template_name = 'mentorski_sustav/polozeni.html'
	context_object_name = 'upisis'
	ordering = ['-id'] 
	
	def get_context_data(self, **kwargs):
		context = super(PopisPolozenihView, self).get_context_data(**kwargs)
		context['status_predmeta'] = Upisi.objects.all().values_list('status')
		return context


class PopisNepolozenihView(ListView):   
	model = Upisi
	template_name = 'mentorski_sustav/nepolozeni.html'
	context_object_name = 'upisis'
	ordering = ['predmet.sem_izvanredni'] 
	

class PopisNepolozenihView(ListView):   
	model = Upisi
	template_name = 'mentorski_sustav/nepolozeni.html'
	context_object_name = 'upisis'
	ordering = ['-id'] 
	

class PopisSvihView(ListView):   
	model = Upisi
	template_name = 'mentorski_sustav/svi_predmeti.html'
	context_object_name = 'upisis'
	ordering = ['-id'] 


class PopisIzbornihView(ListView):   
	model = Predmeti
	template_name = 'mentorski_sustav/izborni.html'
	context_object_name = 'predmetis'
	ordering = ['-id'] 


class PredmetiListView(ListView):   
	model = Predmeti
	template_name = 'mentorski_sustav/predmeti.html'
	context_object_name = 'predmetis'
	#ordering = ['-movieDate']   


class SemestriRedovniListView(ListView):   
	model = Predmeti
	template_name = 'mentorski_sustav/semestri.html'
	context_object_name = 'predmetis'
	ordering = ['kod']  #podesavanje semestara


#############
class ISPIT(DetailView):   
	model = Predmeti
	template_name = 'mentorski_sustav/ispit.html'
	context_object_name = 'predmetis'
	ordering = ['-id'] 
	
	def get_context_data(self, **kwargs):
		context = super(ISPIT, self).get_context_data(**kwargs)
		upisi = Upisi.objects.all()
		context['polozeni'] =Predmeti.objects.filter(upisi__status="polozen").annotate(ups1=Count('upisi')).values_list('ime', 'ups1')
		context['ukupno'] =Predmeti.objects.annotate(ups2=Count('upisi')).values_list('ime', 'ups2')
		return context


class ISPITbroj(DetailView):   
	model = Predmeti
	template_name = 'mentorski_sustav/ispiti_broj.html'
	context_object_name = 'predmetis'
	ordering = ['-id'] 
	
	def get_context_data(self, **kwargs):
		context = super(ISPITbroj, self).get_context_data(**kwargs)
		upisi = Upisi.objects.all()

		#ukupan broj izvanrednih studenata koji su polozili određeni predmet	
		ukupnoIzvanredni =User.objects.filter(upisi__status="polozen", status="Izvanredni", upisi__predmet_id=self.object.id).aggregate(ups2=Count('upisi'))
		context['Suma_izvanrednih'] = sum(ukupnoIzvanredni.values())

		#ukupan broj redovnih studenata koji su polozili određeni predmet	
		ukupnoRedovni =User.objects.filter(upisi__status="polozen", status="Redovni", upisi__predmet_id=self.object.id).aggregate(ups2=Count('upisi'))
		context['Suma_redovnih'] = sum(ukupnoRedovni.values())

		broj_nepolozeni_student =Predmeti.objects.filter(upisi__status="polozen", upisi__predmet_id=self.object.id).aggregate(ups1=Count('upisi'))
		context['broj_nepolozenih_student'] = sum(broj_nepolozeni_student.values())

		context['TOTAL_polozenih'] =Predmeti.objects.filter(upisi__status="polozen", upisi__predmet_id=self.object.id).aggregate(sum_all=Sum('bodovi')).get('sum_all')
		context['TOTAL_polozenih_izvanredni'] =Predmeti.objects.filter(upisi__status="polozen", upisi__predmet_id=self.object.id).aggregate(sum_all=Sum('bodovi')).get('sum_all')  
		return context


class ISPITpolozeni(ListView):   
	model = Predmeti
	template_name = 'mentorski_sustav/linearna.html'
	context_object_name = 'predmetis'
	ordering = ['-id'] 
	
	def get_context_data(self, **kwargs):
		context = super(ISPITpolozeni, self).get_context_data(**kwargs)
		upisi = Upisi.objects.all()

		#ukupan broj izvanrednih studenata koji su polozili određeni predmet	
		ukupnoIzvanredni =User.objects.filter(upisi__status="polozen", status="Izvanredni", upisi__predmet_id=2).aggregate(ups2=Count('upisi'))
		context['Suma_izvanrednih'] = sum(ukupnoIzvanredni.values())

		#ukupan broj redovnih studenata koji su polozili određeni predmet	
		ukupnoRedovni =User.objects.filter(upisi__status="polozen", status="Redovni", upisi__predmet_id=2).aggregate(ups2=Count('upisi'))
		context['Suma_redovnih'] = sum(ukupnoRedovni.values())
		return context


class ISPITimena(ListView):   
	model = Predmeti
	template_name = 'mentorski_sustav/imena.html'
	context_object_name = 'predmetis'
	ordering = ['-id'] 
	
	def get_context_data(self, **kwargs):
		context = super(ISPITimena, self).get_context_data(**kwargs)
		upisi = Upisi.objects.all()

		#imena izvanrednih studenata koji su polozili određeni predmet	
		context['ime_izvanrednih'] =User.objects.filter(upisi__status="polozen", status="Izvanredni", upisi__predmet_id=2).annotate(ups1=Count('upisi'))
		
		#imena redovnih studenata koji su polozili određeni predmet	
		context['ime_redovnih'] =User.objects.filter(upisi__status="polozen", status="Redovni", upisi__predmet_id=2).annotate(ups1=Count('upisi'))
		return context

#####


class UkupnaSumaECTSListView(ListView):   
	model = Predmeti
	template_name = 'mentorski_sustav/ECTS_total.html'
	context_object_name = 'predmetis'
	ordering = ['-id'] 
	
	def get_context_data(self, **kwargs):
		context = super(UkupnaSumaECTSListView, self).get_context_data(**kwargs)

		#Ukupni broj predmeta
		counter = Predmeti.objects.aggregate(total_source1=Count('id'))
		context['Suma_predmeta'] = sum(counter.values())

		#ukupna broj ECTS
		context['Suma_bodova'] = Predmeti.objects.all().aggregate(sum_all=Sum('bodovi')).get('sum_all') 

		#vraca mi id i samo jedinice NE RADI
		context['Suma_semestar'] = Predmeti.objects.values('sem_redovni').order_by('sem_redovni').annotate(Count("sem_redovni")).values_list('id', 'sem_redovni__count')

		#pojedinacna suma REDOVNI
		context['pojedinacna_suma_REDOVNI'] = Predmeti.objects.values('sem_redovni').order_by('sem_redovni').annotate(sum_all=Sum('bodovi')).values_list('sem_redovni', 'sum_all')

		#pojedinacna suma IZVANREDNI
		context['pojedinacna_suma_IZVANREDNI'] = Predmeti.objects.values('sem_izvanredni').order_by('sem_izvanredni').annotate(sum_all=Sum('bodovi')).values_list('sem_izvanredni', 'sum_all')
		return context


class StudentKriterijListView(ListView):   
	model = Predmeti
	template_name = 'mentorski_sustav/ECTS_kriterij.html.'
	context_object_name = 'predmetis'
	ordering = ['-id'] 
	
	def get_context_data(self, **kwargs):
		context = super(StudentKriterijListView, self).get_context_data(**kwargs)

		#nepolozeni/polozeni predmeti
		upisi = Upisi.objects.all()

		#pojedinačna suma nepolozenih
		context['nepolozeni'] =Predmeti.objects.filter(upisi__status="nepolozen").annotate(ups1=Count('upisi')).order_by('id')
		#ukupna suma nepolozenih
		broj_nepolozenih =Predmeti.objects.filter(upisi__status="nepolozen").aggregate(ups1=Count('upisi'))
		context['Suma_nepolozenih'] = sum(broj_nepolozenih.values())

		#ukupan broj nepolozenih za određenog studenta
		broj_nepolozeni_student =Predmeti.objects.filter(upisi__status="nepolozen", upisi__student_id=9).aggregate(ups1=Count('upisi'))
		context['broj_nepolozenih_student'] = sum(broj_nepolozeni_student.values())

		#ukupan broj POLOZENIH za određenog studenta
		broj_POLOZENIH_student =Predmeti.objects.filter(upisi__status="polozen", upisi__student_id=9).aggregate(ups1=Count('upisi'))
		context['broj_POLOZENIH_student'] = sum(broj_POLOZENIH_student.values())

		#pojedinačna suma polozenih
		context['polozeni'] =Predmeti.objects.filter(upisi__status="polozen").annotate(ups1=Count('upisi')).order_by('id')
		#ukupna broj polozenih
		broj_polozenih =Predmeti.objects.filter(upisi__status="polozen").aggregate(ups1=Count('upisi'))
		context['Suma_polozenih'] = sum(broj_polozenih.values())
		
		#ugnjezdavanje npr ispis svi predmeta koji imaju vise od 6 ects i kojei su upisani za određenog studenta
		context['upisi'] = Upisi.objects.all().values_list('predmet', 'status', 'student') 
		context['predmeti'] = Predmeti.objects.all().values_list('id', 'ime', 'bodovi') 
		context['studenti'] = User.objects.all().values_list('id', 'username', 'role')

		#ukupna suma svih polozenih ECTS bodova za određenog studenta (test) RADI
		context['TOTAL_polozenih'] =Predmeti.objects.filter(upisi__status="polozen", upisi__student_id=9).aggregate(sum_all=Sum('bodovi')).get('sum_all') 
		context['TOTAL_polozenih_2'] =Predmeti.objects.filter(upisi__status="polozen", upisi__student_id=9, sem_redovni=2).aggregate(sum_all=Sum('bodovi')).get('sum_all') 
		context['TOTAL_polozenih_1'] =Predmeti.objects.filter(upisi__status="polozen", upisi__student_id=9, sem_redovni=1).aggregate(sum_all=Sum('bodovi')).get('sum_all') 
		context['TOTAL_polozenih_3'] =Predmeti.objects.filter(upisi__status="polozen", upisi__student_id=9, sem_redovni=3).aggregate(sum_all=Sum('bodovi')).get('sum_all') 
		context['TOTAL_polozenih_4'] =Predmeti.objects.filter(upisi__status="polozen", upisi__student_id=9, sem_redovni=4).aggregate(sum_all=Sum('bodovi')).get('sum_all') 
		context['TOTAL_polozenih_5'] =Predmeti.objects.filter(upisi__status="polozen", upisi__student_id=9, sem_redovni=5).aggregate(sum_all=Sum('bodovi')).get('sum_all') 
		context['TOTAL_polozenih_6'] =Predmeti.objects.filter(upisi__status="polozen", upisi__student_id=9, sem_redovni=6).aggregate(sum_all=Sum('bodovi')).get('sum_all') 
		context['TOTAL_polozenih_7'] =Predmeti.objects.filter(upisi__status="polozen", upisi__student_id=9, sem_redovni=7).aggregate(sum_all=Sum('bodovi')).get('sum_all') 
		context['TOTAL_polozenih_8'] =Predmeti.objects.filter(upisi__status="polozen", upisi__student_id=9, sem_redovni=8).aggregate(sum_all=Sum('bodovi')).get('sum_all') 
		#context['TOTAL_polozenih_ECTS'] = sum(TOTAL.sum_all.values())
		return context


class SemestriIzvanredniListView(ListView):   
	model = Predmeti
	template_name = 'mentorski_sustav/semestri.html'
	context_object_name = 'predmetis'
	ordering = ['sem_izvanredni']  #podesavanje semestara


class SemestriPKListView(ListView):   
	model = Predmeti
	template_name = 'mentorski_sustav/semestar_predmet.html'
	context_object_name = 'predmetis'
	ordering = ['-sem_izvanredni']


class SemestriPK2ListView(ListView):   
	model = Predmeti
	template_name = 'mentorski_sustav/semestar_predmet.html'
	context_object_name = 'predmetis'
	ordering = ['-sem_izvanredni']


class SemestriDetailView(DetailView):
	model = Predmeti
	template_name='mentorski_sustav/semestri_detail.html/'
	context_object_name = 'predmetis'


class PredmetiDetailView(SuccessMessageMixin, FormMixin, DetailView):
	model = Predmeti
	form_class = UpisiForm
	success_message = "Status uspiješno promijenjen!"
	template_name='mentorski_sustav/predmeti_detail.html/'
	context_object_name = 'predmetis'
	ordering = ['-kod']

	def get_success_url(self):
		return reverse('mentorski-predmeti_detail', kwargs={'pk': self.object.id})
    
	def get_context_data(self, **kwargs):
		context = super(PredmetiDetailView, self).get_context_data(**kwargs)
		context['form'] = UpisiForm(initial={'predmet': self.object, 'student': self.request.user })
		context['test'] = Upisi.objects.all().values_list('predmet', 'status', 'student') 
		return context

	##ne znam jel mi ovo sve uopce potrebno (testiraj)	
	def post(self, request, *args, **kwargs):
		self.object = self.get_object()
		form = self.get_form()
		if form.is_valid():
			return self.form_valid(form)
		else:
			messages.warning(request, 'Izabrana opcija već postoji u sustavu!')
			return self.form_invalid(form)

	def form_valid(self, form):
		form.instance.user = self.request.user
		Upisi.objects.filter(id= self.object.id).update(status=form.instance.status) 
		form.instance.save()
		return super(PredmetiDetailView, self).form_valid(form)


class StatusDetailView(DetailView):
	model = Predmeti
	#form_class = TicketForm
	#success_message = "Ticket successfully purchased !"
	template_name='mentorski_sustav/predmeti_detail.html/'
	context_object_name = 'predmetis'


class StudentiListView(ListView):  
	model = User
	template_name = 'mentorski_sustav/user.html'
	context_object_name = 'users'
	#ordering = ['-movieDate']         #sortiranje podataka(filmova)


class StudentiDetailView(DetailView):
	model = User
	#form_class = TicketForm
	#success_message = "Ticket successfully purchased !"
	template_name='mentorski_sustav/user_detail.html'
	context_object_name = 'users'


	def get_context_data(self, **kwargs):
		context = super(StudentiDetailView, self).get_context_data(**kwargs)	
		context['upisi'] = Upisi.objects.all().values_list('predmet', 'status', 'student') 
		context['predmeti'] = Predmeti.objects.all().values_list('id', 'ime', 'bodovi', 'izborni') 
		return context


class PredmetiUpdateView(UpdateView):
	model = Predmeti
	fields = ['ime', 'kod', 'program', 'bodovi', 'izborni']

	def form_valid(self, form):
		form.instance.user = self.request.user  
		return super().form_valid(form)


class UserUpdateView(UpdateView):
	model = User
	fields = ['first_name', 'last_name', 'email', 'role', 'status']

	def form_valid(self, form):
		form.instance.user = self.request.user  
		return super().form_valid(form)


class PredmetiCreateView(CreateView):
	model = Predmeti
	fields = ['ime', 'kod', 'program', 'bodovi', 'sem_redovni', 'sem_izvanredni', 'izborni']  #probaj rename-at polja

	def form_valid(self, form):
		form.instance.user = self.request.user  #namjestanje da trenutni autor bude trenutni ulogirani user
		return super().form_valid(form)   	


class UserCreateView(CreateView):
	model = User
	fields = ['first_name', 'last_name', 'username', 'email', 'password', 'role', 'status']  #probaj rename-at polja
	labels = {
            "first_name": "I have a custom label",
        }

	def form_valid(self, form):
		form.instance.user = self.request.user  #namjestanje da trenutni autor bude trenutni ulogirani user
		return super().form_valid(form)   	


class UpisPredmetaView(CreateView): #Student upis predmeta
	model = Upisi
	success_message = "Upiješan upis!"
	fields = ['predmet', 'student', 'status']  

	def get_context_data(self, **kwargs):
		context = super(UpisPredmetaView, self).get_context_data(**kwargs)
		context['form'] = UpisPredmetaForm(initial={ 'student': self.request.user }, student=self.request.user)
		return context


class UpisStudentaView(CreateView):  #Mentor i Admin upis predmeta studentu
	model = Upisi
	fields = ['predmet', 'student', 'status']  

	def get_context_data(self, **kwargs):  
		context = super(UpisStudentaView, self).get_context_data(**kwargs)
		context['form'] = UpisiForm(student=self.request.user) #########
		return context


class UpisiUpdateView(UpdateView):
	slug_field = 'predmet_id'
	slug_url_kwarg = 'predmet_id'
	model = Upisi
	template_name='mentorski_sustav/promijena_statusa.html'
	fields = ['status']

	def get_queryset(self):
		return Upisi.objects.filter(student=self.request.user)


class IspisStudentaView(LoginRequiredMixin, DeleteView):
	slug_field = 'predmet_id'
	slug_url_kwarg = 'predmet_id'
	model = Upisi
	template_name='mentorski_sustav/test.html'   
	#def get_schedule(self, **kwargs):
     #   return get_object_or_404(User, id=kwargs['pk'])

	def get_queryset(self, **kwargs):
		return Upisi.objects.select_related('student').filter(student_id=9)   #RADI (multy object) 'predmet': self.object,
		#id = self.request.query_params.get('id')
  		#return Upisi.objects.filter(student=self.object.student_id)   #treba mi id trenutnoG studenta nad kojem zelim brisat upisani predmet
  		#return Upisi.objects.select_related('student').filter(kwargs={'student_id': self.request.GET.get('student')}) 
  		#return Upisi.objects.select_related('student').filter(student_id=10)   #hardcode RADI
		#test=(r"/mentorski_sustav/([0-9])/user/?", StudentiDetailView)
  		#test=(r"/mentorski_sustav/([0-9])/?/user/", StudentiDetailView)
  		#return Upisi.objects.filter(student__id=10) #hardcode RAD
  		#std_id = kwargs['pk']
  		#return Upisi.objects.filter(student__id=) 
  		#return User.objects.filter(id=9) 
  		#return Upisi.objects.filter(student=user.id)

	def get_success_url(self):		
		return reverse_lazy('mentorski-user_detail', kwargs={'pk': self.object.student_id})


class UserDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = User
    success_url = '/'

    def test_func(self):
        predmeti = self.get_object()
        #if self.request.user == projection.user:
        if self.request.user == self.request.user:
            return True
        return False


class PredmetiDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Predmeti
    success_url = '/'

    def test_func(self):
        predmeti = self.get_object()
        #if self.request.user == projection.user:
        if self.request.user == self.request.user:
            return True
        return False


class IspisDeleteView(LoginRequiredMixin, DeleteView):
	slug_field = 'predmet_id'
	slug_url_kwarg = 'predmet_id'
	model = Upisi
	template_name='mentorski_sustav/delete.html'
  
	def get_queryset(self):
  		return Upisi.objects.filter(student=self.request.user)

	def get_success_url(self):		
		return reverse_lazy('mentorski-predmeti_detail', kwargs={'pk': self.object.predmet_id})

